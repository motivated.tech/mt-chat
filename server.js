const WebSocket = require('ws')

const wss = new WebSocket.Server({
	perMessageDeflate: false,
	port: 8081
})

wss.broadcast = (data) => {
	wss.clients.forEach((client) => {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data)
		}
	})
}

wss.on('connection', (ws) => {

	ws.on('message', (message) => {

		let parsedMessage = JSON.parse(message)

		switch (parsedMessage.type) {
			case "chatMessage": {
				wss.broadcast(JSON.stringify({
					type:'chatMessage',
					data: parsedMessage.data
				}))
			}
			break
		}

		
	})
})

